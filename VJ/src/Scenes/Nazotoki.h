//
//  魔法のステッキで色をスプラトゥーン的に塗りつぶせるシーン
//


#pragma once

#include "ofxState.h"

class Nazotoki : public itg::ofxState<SharedData>
{
public:
    
   
    
    string getName()
    {
        return FILE;
    }
    
    void setup()
    {
        
    }
    
    void update()
    {
        getSharedData().fbo.begin();
        {
            
            // clear
            ofBackground(0);
            
            
            // Kinectの位置を取得
            ofxJSONElement positions = getSharedData().performerPositions;
            ofColor(255);
            
            for(auto pos : positions)
            {
                ofCircle( pos["x"].asFloat(), pos["y"].asFloat(), pos["width"].asFloat()*0.2);
            }
            
            //
            ofSetColor(255);
            string msg = "Nazotoki";
            ofDrawBitmapString(msg, 10, 40);
            
        }
        getSharedData().fbo.end();
    }
    
    
};
