//
//  Osc Manager
//

#pragma once

#include "ofxOsc.h"


class OscManager
{
public:
    
    // vars
    ofxOscReceiver _oscKinectReceiver;
    ofxOscReceiver _oscIrReciver;
    ofxOscReceiver _oscMaxReceiver;
    ofxOscSender _oscMaxSender;
    
    
    void setup()
    {
        // kinect reciever
        _oscKinectReceiver.setup(Const::OSC_KINECT_SERVER_PORT);

        // ir reciever
        _oscIrReciver.setup(Const::OSC_IR_SERVER_PORT);
    
        // max reciever
        _oscMaxReceiver.setup(Const::OSC_MAX_SERVER_PORT);
    
        // max clent
        _oscMaxSender.setup("127.0.0.1", Const::OSC_MAX_CLIENT_PORT );
        ofxOscMessage m;
        m.setAddress("/start");
        _oscMaxSender.sendMessage(m);
    }
    
    
    // maxのパラメータをパース
    ParamObj updateMax()
    {
        ParamObj params;
    
        
        while(_oscMaxReceiver.hasWaitingMessages())
        {
            // parse osc
            ofxOscMessage m;
            _oscMaxReceiver.getNextMessage(&m);
            string key = m.getAddress().erase(0,1);
            
            //型チェック
            switch( m.getArgType(0) )
            {
                // OSCから拾った値をパラメータにセット
                case OFXOSC_TYPE_INT32:
                case OFXOSC_TYPE_INT64:
                default:
                {
                    int val = m.getArgAsInt32(0);
                    params[key] = val;
                    break;
                }
                    
                case OFXOSC_TYPE_FLOAT:
                {
                    float val2 = m.getArgAsFloat(0);
                    params[key] = val2;
                    break;
                }
            }
            

            // 確認
            cout << key << " : " << params[key] << "," << endl;
        }
        
        if(!params.empty()) cout << "--- end osc --" << endl;
                
        return params;
    }
    
    
    // Kinectのパラメータをパース
    ofxJSONElement updateKinect()
    {
        // static
        static ofxJSONElement pJson1; //１個前のデータ
        static ofxJSONElement pJson2; //２個前のデータ
        static int count = 0; //補完回数
        static const int max_count = 15; //補完回数のmax
        
        // vars
        ofxJSONElement json;
        
        // Kinect用WinPCからOSCを受信
        while( _oscKinectReceiver.hasWaitingMessages() )
        {
            // parse osc
            ofxOscMessage m;
            _oscKinectReceiver.getNextMessage(&m);
            string key = m.getAddress().erase(0,1);
            string val = m.getArgAsString(0);

//            cout << key << ":" << val << endl;
            
            // add
            if( key == "data" )
            {
                json.parse(val);
            }
            
            // pJson1、pJson2をアップデート
            pJson2 = pJson1;
            pJson1 = json;
            count = 0;
        }
        
        
        // いまKinectからデータが来なかった場合
        if( json.empty()
        && count < max_count
        && !pJson1.empty()
        && !pJson2.empty())
        {
            // 過去のデータから、現在のデータを補完する
            float rate = 1.0;
            for (auto prej : pJson1 )
            {
                string key = ofToString( prej["id"] );
                
                // ２個前のデータに同じラベルのがあったら
                if ( !pJson2[key].empty() )
                {
                    //
                    ofxJSONElement ele;
                    ele["id"] = key;
                    ele["x"] = (pJson1[key]["x"].asFloat() - pJson2[key]["x"].asFloat()) * rate + pJson1[key]["x"].asFloat() ;
                    ele["y"] = (pJson1[key]["y"].asFloat() - pJson2[key]["y"].asFloat()) * rate + pJson1[key]["y"].asFloat() ;
                    ele["width"] = (pJson1[key]["width"].asFloat() - pJson2[key]["width"].asFloat()) * rate + pJson1[key]["width"].asFloat() ;
                    ele["height"] = (pJson1[key]["height"].asFloat() - pJson2[key]["height"].asFloat()) * rate + pJson1[key]["height"].asFloat() ;
                    
                    //
                    json[ofToString(key)] = ele;
                }
            }
        
            if( json.empty() )
            {
                json = pJson1;
            }
            
            //cout << "ほかん:"  << json.size() <<  endl;
        
            // pJson1とpJson2に値をアップデート
            pJson2 = pJson1;
            pJson1 = json;
            
            // 補完カウントがある一定以上を超えたら、切れたとみなして、何も返さない
            count++;
        }
        // あんまりにも補完が続いたら、データ終了のお知らせ
        else if ( max_count <= count  )
        {
            count = 0;
            pJson1 = ofxJSONElement();
            pJson2 = ofxJSONElement();
        }
//        // たんじゅんにemptyになるとき
//        else if( json.empty() )
//        {
//            cout << "emptyいき" << endl;
//            cout << count << ":" << pJson1.empty() << ":" << pJson2.empty() << endl ;
//        }
        
        return json;
    }
    
    
    // IRのパラメータをパース
    vector<Particle> updateIR()
    {
        vector<Particle> irHands;
        
        //ofxJSONElement json;
        while( _oscIrReciver.hasWaitingMessages())
        {
            // parse osc
            ofxOscMessage m;
            _oscIrReciver.getNextMessage(&m);
            
            //keyをチェック
            string key = m.getAddress();
            if( key != "/mouse/position4" )
            {
                return;
            }
            
            int mousenum = m.getNumArgs();
            for (int i = 0 ; i < mousenum / 3 ; i++)
            {
                // parse data
                int irId = m.getArgAsInt32(0 + i*3);
                int irX = m.getArgAsInt32(1 + i*3);
                int irY = m.getArgAsInt32(2 + i*3);
            
                // add
                Particle p;
                p.setup(irX, irY, irId);
                irHands.push_back(p);
            }
        }
        
        return irHands;
    }
};

