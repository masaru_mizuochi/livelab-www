//
//  DummyKinectManager
//

#pragma once

#include "Particle.h"

class DummyKinectManager
{
public:
    
    // const static
    static const int NUM = 4;
    float MAX_VEC = 0.1;

    // vars
    vector<Particle> _particles;

    void setup()
    {
        // particles
        for(int i = 0 ; i < NUM; i++)
        {
            Particle p;
            p.friction = 0.0005;
            float posX = ofRandom(0, ofGetWidth());
            float posY = ofRandom(0, ofGetHeight());
            float vecX = ofRandom(-0.03, 0.03);
            float vecY = ofRandom(-0.03, 0.03);
            
            p.setup(posX, posY, vecX, vecY);
            _particles.push_back(p);
        }
    }
    
    // Kinectのパラメータをパース
    ofxJSONElement update()
    {
        ofxJSONElement json;
        int frame_num = ofGetFrameNum();
        int len = _particles.size();
    
        for( int i = 0; i < len ; i++ )
        {
            // update
            _particles[i].update();
            _particles[i].resetForce();
            
            // 引力の位置
            float x = ofNoise(frame_num*0.01) * ofGetWidth();
            float y = ofNoise(frame_num*0.02) * ofGetHeight() ;
            float power = abs(ofNoise(frame_num*0.002)) * MAX_VEC ;
            _particles[i].addAttractionForce( x,  y, ofGetWidth(), power);
            
            // 壁との跳ね返り
            _particles[i].bounceOffWalls();
            
            //
            ofxJSONElement ele;
            ele["id"] = i;
            ele["x"] = _particles[i].position.x;
            ele["y"] = _particles[i].position.y;
            ele["width"] = 50;
            ele["height"] = 50;
            
            // add
            json[i] = ele;
        }

        return json;
    }
    
};
