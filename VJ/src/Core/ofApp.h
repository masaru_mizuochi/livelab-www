#pragma once

#include "ofMain.h"

// addon
#include "ofxStateMachine.h"
#include "ofxPostGlitch.h"
#include "ofxJSON.h"
//#include "ofxSyphon.h"

// DataType
#include "EffectName.h"
#include "Particle.h"

// Core
#include "Const.h"
#include "SharedData.h"

// Connector
#include "OscManager.h"
#include "DummyKinectManager.h"



// Scenes
#include "Test.h"
#include "Genkidama.h"
#include "Dokuro.h"
#include "Splatoon.h"
#include "RythmGame.h"
#include "Nazotoki.h"
#include "Lyric.h"

class ofApp : public ofBaseApp
{
    
public:
    void setup();
    void update();
    void draw();
    void keyPressed(int key);
    void keyReleased(int key);
    void audioIn(float * input, int bufferSize, int nChannels);
    
    // my functions
    void changeSceneAt(string sceneName);
    void changeEffectSetting(string effectName ,int flag);

    // vars
    ofxPostGlitch _effect;
    itg::ofxStateMachine<SharedData> _state;
    OscManager _oscManger;
    DummyKinectManager _dummyKinectManager;
    
    //ofxSyphonServer _syphon;
    
    // volume
    ofSoundStream _sound;
    float _volume;
    
    // datas
    map<string, string> _sceneNames;
    vector<EffectName> _effectNames;

};
