//
//  シーン共通のシングルトンデータ（共有変数系）
//


#pragma once

class SharedData
{
public:
    
    // 描画データ
	ofFbo fbo;
    
    // 音量
    float volume;
    
    // maxからのパラメータ
    ParamObj params;
    
    // kinectからのパフォーマー位置パラメータ
    ofxJSONElement performerPositions;
    
    // IRからのお客さんの腕の位置
    vector<Particle> irHands;
};
