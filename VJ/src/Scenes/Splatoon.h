//
//  魔法のステッキで色をスプラトゥーン的に塗りつぶせるシーン
//


#pragma once

#include "ofxState.h"

class Splatoon : public itg::ofxState<SharedData>
{
public:
    
    ofFbo _preDisplayFbo;
    
    string getName()
    {
        return FILE;
    }
    
    void setup()
    {
        _preDisplayFbo.allocate(Const::WIDTH, Const::HEIGHT, GL_RGBA, 4);
    }
    
    
    void  stateEnter()
    {
        _preDisplayFbo = getSharedData().fbo;
    }

    void update()
    {
        getSharedData().fbo.begin();
        {
            
            // clear
            ofBackground(0);
            
            // 前画面のドクロ＋悪口の画面を描画する
            _preDisplayFbo.draw(0,0);
            
            
            // IRの位置を取得
            vector<Particle> irHands = getSharedData().irHands;
            ofColor(255);
            for(Particle irHand : irHands)
            {
                ofCircle( irHand.position.x, irHand.position.y, 10.0);
            }
            
            //
            ofSetColor(255);
            stringstream msg;
            msg << "splatoon" << getSharedData().params["scene_mode"] << endl;
            ofDrawBitmapString(msg.str(), 10, 40);
            
        }
        getSharedData().fbo.end();
    }
    
    
};
