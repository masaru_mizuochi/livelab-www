//
//  KinectManager.h
//  ３次元パーティクルクラス
//
//  Created by 貴田　達也 on 7/18/15.
//


#pragma once
#include "ofMain.h"

class Particle3D
{

public:
    
    //id
    int tex_id;
    
    // 位置ベクトル
    ofVec3f position;
    
    // 速度ベクトル
    ofVec3f velocity;
    
    // 力ベクトル
    ofVec3f force;
    
    //カメラとの距離
    float cam_dist;

    //
    int count;
    

    
    //コンストラクタ
    Particle3D()
    {
        count = 100;
    }
    
    // 初期設定
    void setup(int i, float camdist)
    {
        cam_dist = camdist;
        
        tex_id = i;
        
        reset();
    }
   
    // 位置の更新
    void updatePos()
    {
        position.x = position.x * velocity.x;
        position.y = position.y * velocity.y;
        position.z = position.z * velocity.z;
        
        
        if( position.x < 0.5 && position.y < 0.5 && position.z < 0.5 )
        {
            reset();
        }

    }

    // 位置のリフレッシュ
    void reset()
    {
        ofSeedRandom();
        float t1 = ofRandom(3.14);
        float t2 = ofRandom(3.14);
        position = ofVec3f(cam_dist * sin(t1) * cos( t2 )
                           ,cam_dist * sin(t1) * sin( t2 )
                           ,cam_dist * cos(t1));
        
        float r_max = 0.99;
        float r_min = 0.9;
        velocity = ofVec3f(ofRandom(r_min,r_max), ofRandom(r_min,r_max), ofRandom(r_min,r_max) );
    }
    
};