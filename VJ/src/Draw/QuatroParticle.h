#pragma once
#include "ofMain.h"

class QuatroParticle {

public:
    /**
     * methods
     */
    
    //コンストラクタ
    QuatroParticle();
    // 初期設定
    void setup(ofVec2f position, ofVec2f velocity, ofFloatColor color ,float size, int life_count, int texture_id);
//    void setup(float positionX, float positionY, float velocityX, float velocityY);
    
    // 力をリセット
    void resetForce();
    
    // 力を加える
    void addForce(ofVec2f force);
    void addForce(float forceX, float forceY);
    
    // 力を更新
    void updateForce();
    
    // 位置の更新
    void updatePos();
    
    // 更新(位置と力)
    void update();
    
    // 画面からはみ出たらバウンドさせる
    void bounceOffWalls(ofRectangle rect);
    
    // 画面からはみ出たら反対側から出現
    void throughOfWalls(ofRectangle rect);
    
    // 描画
    void draw();
    
    
    // 反発する力
    void addRepulsionForce(float x, float y, float radius, float scale);
    // 反発する力 - Particle版
    void addRepulsionForce(QuatroParticle &p, float radius, float scale);
    // 引きつけあう力
    void addAttractionForce(float x, float y, float radius, float scale);
    // 引き付けあう力 - Particle版
    void addAttractionForce(QuatroParticle &p, float radius, float scale);
    
    
    
    /**
     * vars
     */

    // 位置ベクトル
    ofVec2f position;
    // 速度ベクトル
    ofVec2f velocity;
    // 力ベクトル
    ofVec2f force;
    // 摩擦係数
    float friction;
    // パーティクルの半径
    float radius;
    // 固定するかどうか
    bool bFixed;
    // パーティクルの質量
    float mass;
    // 色
    ofFloatColor color;
    
    //サイズ
    float size;
    float max_size;
    //テクスチャID
    int texture_id;
    //カウント
    int count;
    int life_count;
};