#pragma once

#include "ofMain.h"

// native lib
#include <Kinect.h>

// of addon
#include "ofxOsc.h"
#include "ofxCv.h"
#include "ofxGui.h"
#include "ofxJson.h"
#include "ofxQuadWarp.h"



// my lib
#include "Const.h"


class ofApp : public ofBaseApp
{

public:
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	bool setupKinect();



	// static const
	float SCALE_COLUMN1; //Kinect�摜�̊g�嗦
    string OSC_CLIENT_IP; // �U�� IP


	// vars
	IKinectSensor * _kinect;
	IDepthFrameReader * _depthFrameReader;


	// vars
	//unsigned int _bufferSize;
	//cv::Mat _bufferMat;
	//cv::Mat _depthMat;
	ofImage _img;
	ofxCv::ContourFinder _contour;
	ofxQuadWarp _warper;
	ofxOscSender _osc_sender;
	ofFbo _cv_fbo;


	// vars for UI
    ofxPanel _gui;
	ofParameter <int> _p_kinect_min_dist;
	ofParameter <int> _p_kinect_max_dist;
    ofParameter <int> _p_blur;
    ofParameter <int> _p_cv_min_area;
    ofParameter <int> _p_cv_max_area;
    ofParameter <int> _p_cv_threshold;
    ofParameter <int> _p_cv_persistance;
    ofParameter <int> _p_cv_max_dist;
    ofxLabel _p_osc_label;
    ofxToggle _p_start_osc;

	
	// vars for FPS
	float _kinectFps;
	float _preElapsedTime;


};


// Safe release for interfaces
template<class Interface>
inline void SafeRelease(Interface *& pInterfaceToRelease)
{
    if (pInterfaceToRelease != NULL)
    {
        pInterfaceToRelease->Release();
        pInterfaceToRelease = NULL;
    }
}