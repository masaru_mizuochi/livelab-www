
autowatch;

// ------ //
// jsons
// ------ //
var _params = {
// シーン名

	"dokuro" : 
	{
		scene_id : 1,
		scene_mode : 1,
		
	kinect_cam_theta2 :90.0582,
	kinect_cam_theta : 22.2353,
	kinect_cam_r : 414.706,
	},

	"nazotoki" : 
	{
		scene_id : 2,
		scene_mode : 1,
	},

	"splatoon1" : 
	{
		scene_id :3,
		scene_mode : 1,
	},

	"splatoon2" : 
	{
		scene_id : 3,
		scene_mode : 2,
	},
	
	"lyric1" : 
	{
		scene_id : 4,
		scene_mode : 1,
	},

	"lyric2" : 
	{
		scene_id : 4,
		scene_mode : 2,
	},
	

	"rythmgame" : 
	{
		scene_id : 5,
		scene_mode : 1,
	},


	"test" : 
	{
		scene_id : 6,
		scene_mode : 1,
	},

	"genkidama" : 
	{
		scene_id : 7,
		scene_mode : 1,
	},

};


var _mins = {
	scene_id : 0,
	scene_mode : 0,
	
	volume_gain : 0,
	
	effect_inv_grad : 0,
	effect_dot : 0,
	effect_glow : 0,
	effect_rgb : 0,
	
	ir_dummy : 0,
	kinect_dummy : 0,
	
	kinect_cam_theta2 : 0,
	kinect_cam_theta : 0,
	kinect_cam_r : 0,
};


// ------ //
// event handlers
// ------ //
// シーン系
function test() { sceneChangeAt("test"); }
function genkidama() { sceneChangeAt("genkidama"); }
function dokuro() { sceneChangeAt("dokuro"); }
function splatoon1() { sceneChangeAt("splatoon1"); }
function splatoon2() { sceneChangeAt("splatoon2"); }
function rythmgame() { sceneChangeAt("rythmgame"); }
function nazotoki() { sceneChangeAt("nazotoki"); }
function lyric1() { sceneChangeAt("lyric1"); }
function lyric2() { sceneChangeAt("lyric2"); }

// ------ //
// my functions
// ------ //
function sceneChangeAt(scene_id)
{
	post("sceneChangeAt:", scene_id, "\n");
	for(var key in _params[scene_id])
	{
		var p = patcher.getnamed( key );
			p.setvalueof( _params[scene_id][key] - _mins[key] );
	}
}
