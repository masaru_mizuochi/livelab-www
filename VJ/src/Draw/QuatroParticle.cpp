#include "QuatroParticle.h"

QuatroParticle::QuatroParticle()
{
    radius = 2.0;
    friction = 0.01;
    mass = 1.0;
    bFixed = false;
//    color.setHsb(0.0, 1.0, 1.0);
    size = 20.0;
    texture_id = 0;
    count = 0;
}

void QuatroParticle::setup(ofVec2f _position, ofVec2f _velocity, ofFloatColor _color, float _size, int _life_count, int _texture_id)
{
    // 位置を設定
    position = _position;
    
    // 初期速度を設定
    velocity = _velocity;
    
    // 色を設定
    color = _color;

    // サイズを設定
    size = 0;
    max_size = _size;
    
    // 寿命のカウント
    life_count = _life_count;
    
    // テクスチャID
    texture_id = _texture_id;
}


// 力をリセット
void QuatroParticle::resetForce()
{
    force.set(0, 0);
}

// 力を加える
void QuatroParticle::addForce(ofVec2f _force)
{
    force += _force / mass;
}
void QuatroParticle::addForce(float forceX, float forceY)
{
    force += ofVec2f(forceX, forceY);
}

// 摩擦力の更新
void QuatroParticle::updateForce()
{
    force -= velocity * friction;
}

// 位置の更新
void QuatroParticle::updatePos()
{
    if (bFixed)
    {
        return;
    }
    
    velocity += force;
    position += velocity;
}

// 力の更新と座標の更新をupdateとしてまとめる
void QuatroParticle::update()
{
    updateForce();
    updatePos();
    
    
    //
    float animation_count = 20;
    int rest_count = count - (life_count - animation_count);
    if( count < animation_count )
    {
        size = max_size * count/animation_count;
        color.setHsb(count/animation_count, count/animation_count, 1.0);
    }
    else if( 0 < rest_count )
    {
        size = max_size * (1 - rest_count/animation_count);
        color.setHsb(0.0, 1.0, 1.0);
    }
    else
    {
        size = max_size;
    }
    
    
    
    
    
    
    // increment
    count++ ;
}


// 画面の端でバウンドする(改定版)
void QuatroParticle::bounceOffWalls(ofRectangle rect){
    bool bDampedOnCollision = false;
    bool bDidICollide = false;
    
    float minx = rect.x;
    float miny = rect.y;
    float maxx = rect.x + rect.width;
    float maxy = rect.y + rect.height;
    
    if (position.x > maxx){
        position.x = maxx;
        velocity.x *= -1;
        bDidICollide = true;
    } else if (position.x < minx){
        position.x = minx;
        velocity.x *= -1;
        bDidICollide = true;
    }
    
    if (position.y  > maxy){
        position.y = maxy;
        velocity.y *= -1;
        bDidICollide = true;
    } else if (position.y  < miny){
        position.y = miny;
        velocity.y *= -1;
        bDidICollide = true;
    }
    
    if (bDidICollide == true && bDampedOnCollision == true){
        velocity *= 0.3;
    }
}

void QuatroParticle::throughOfWalls(ofRectangle rect){
    float minx = rect.x;
    float miny = rect.y;
    float maxx = rect.x + rect.width;
    float maxy = rect.y + rect.height;
    if (position.x < minx) {
        position.x = maxx;
    }
    if (position.y < miny) {
        position.y = maxy;
    }
    if (position.x > maxx) {
        position.x = minx;
    }
    if (position.y > maxy) {
        position.y = miny;
    }
}

// 描画
void QuatroParticle::draw(){
    ofCircle(position, radius);
}

// 反発する力
void QuatroParticle::addRepulsionForce(float x, float y, float radius, float scale){
    // 力の中心点を設定
    ofVec2f posOfForce;
    posOfForce.set(x,y);
    // パーティクルと力の中心点との距離を計算
    ofVec2f diff = position - posOfForce;
    float length = diff.length();
    // 力が働く範囲かどうか判定する変数
    bool bAmCloseEnough = true;
    // もし設定した半径より外側だったら、計算しない
    if (radius > 0){
        if (length > radius){
            bAmCloseEnough = false;
        }
    }
    // 設定した半径の内側だったら
    if (bAmCloseEnough == true){
        // 距離から点にかかる力ベクトルを計算
        float pct = 1 - (length / radius);
        diff.normalize();
        force.x = force.x + diff.x * scale * pct;
        force.y = force.y + diff.y * scale * pct;
    }
}

// 反発する力 - Particle版
void QuatroParticle::addRepulsionForce(QuatroParticle &p, float radius, float scale){
    // 力の中心点を設定
    ofVec2f posOfForce;
    // Particleへの参照から座標を取得、力の中心に設定
    posOfForce.set(p.position.x,p.position.y);
    // パーティクルと力の中心点との距離を計算
    ofVec2f diff = position - posOfForce;
    float length = diff.length();
    // 力が働く範囲かどうか判定する変数
    bool bAmCloseEnough = true;
    // もし設定した半径より外側だったら、計算しない
    if (radius > 0){
        if (length > radius){
            bAmCloseEnough = false;
        }
    }
    // 設定した半径の内側だったら
    if (bAmCloseEnough == true){
        // 距離から点にかかる力ベクトルを計算
        float pct = 1 - (length / radius);
        diff.normalize();
        force.x = force.x + diff.x * scale * pct;
        force.y = force.y + diff.y * scale * pct;
        // 参照したパーティクルの力を計算して更新
        p.force.x = p.force.x - diff.x * scale * pct;
        p.force.y = p.force.y - diff.y * scale * pct;
    }
}

// 引き付けあう力
void QuatroParticle::addAttractionForce(float x, float y, float radius, float scale){
    // 力の中心点を設定
    ofVec2f posOfForce;
    posOfForce.set(x,y);
    // パーティクルと力の中心点との距離を計算
    ofVec2f diff = position - posOfForce;
    float length = diff.length();
    // 力が働く範囲かどうか判定する変数
    bool bAmCloseEnough = true;
    // もし設定した半径より外側だったら、計算しない
    if (radius > 0){
        if (length > radius){
            bAmCloseEnough = false;
        }
    }
    // 設定した半径の内側だったら
    if (bAmCloseEnough == true){
        // 距離から点にかかる力ベクトルを計算
        float pct = 1 - (length / radius);
        diff.normalize();
        force.x = force.x - diff.x * scale * pct;
        force.y = force.y - diff.y * scale * pct;
    }
}

// 引き付けあう力 - Particle版
void QuatroParticle::addAttractionForce(QuatroParticle &p, float radius, float scale){
    // 力の中心点を設定
    ofVec2f posOfForce;
    // Particleへの参照から座標を取得、力の中心に設定
    posOfForce.set(p.position.x, p.position.y);
    // パーティクルと力の中心点との距離を計算
    ofVec2f diff = position - posOfForce;
    float length = diff.length();
    // 力が働く範囲かどうか判定する変数
    bool bAmCloseEnough = true;
    // もし設定した半径より外側だったら、計算しない
    if (radius > 0){
        if (length > radius){
            bAmCloseEnough = false;
        }
    }
    // 設定した半径の内側だったら
    if (bAmCloseEnough == true){
        // 距離から点にかかる力ベクトルを計算
        float pct = 1 - (length / radius);
        diff.normalize();
        force.x = force.x - diff.x * scale * pct;
        force.y = force.y - diff.y * scale * pct;
        // 参照したパーティクルの力を計算して更新
        p.force.x = p.force.x + diff.x * scale * pct;
        p.force.y = p.force.y + diff.y * scale * pct;
    }
}