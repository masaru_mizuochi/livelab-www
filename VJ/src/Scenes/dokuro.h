//
//  ドクロが出るシーン
//


#pragma once

#include "ofxState.h"



class Dokuro : public itg::ofxState<SharedData>
{
public:
    
    // vars
    ofCamera _cam;
    ofPlanePrimitive _plane;
    ofShader _shader;
    ofImage _texture;
    
    //
    float _cam_r = 500;
    float _cam_theta = 0;
    float _cam_theta2 = 0;

    
    string getName()
    {
        return FILE;
    }
    
    void setup()
    {
        // plane
        _plane.set(640, 480);
        _plane.setPosition(0, 0, 0);
        _plane.setResolution(3, 3);
        
        // shader
        _shader.load("shaders/pointSprite.vert", "shaders/pointSprite.frag");
        
        // texture
        ofDisableArbTex();
            _texture.loadImage("textures/dokuro/dokuro.png");
        ofEnableArbTex();

    }
    
    void update()
    {
        
        // カメラの設定
        ofVec3f center( Const::WIDTH * 0.5, 0, Const::HEIGHT * 0.5);

        float cam_r = getSharedData().params["kinect_cam_r"] ;
        
        ofVec3f cam_position(
            cam_r * sin( getSharedData().params["kinect_cam_theta"]*DEG_TO_RAD ) * cos( getSharedData().params["kinect_cam_theta2"]*DEG_TO_RAD ) + center.x,
            cam_r * sin( getSharedData().params["kinect_cam_theta"]*DEG_TO_RAD ) * sin( getSharedData().params["kinect_cam_theta2"]*DEG_TO_RAD ) + center.y,
            cam_r * cos( getSharedData().params["kinect_cam_theta"]*DEG_TO_RAD ) + center.z
        );
                             
        _cam.setPosition(cam_position);
        _cam.lookAt(center, ofVec3f(0, 1, 0));
        
        
        
        // 描画開始
        getSharedData().fbo.begin();
        {
            
            // clear
            ofBackground(0);
            
            

            
            // カメラスタート
            _cam.begin();
            ofEnableDepthTest();
            {
                // Kinectで分身
                ofColor(0);
                ofPushMatrix();
                {
//                    ofScale(-1.0, 1.0);
                    ofxJSONElement positions = getSharedData().performerPositions;
                    
                    ofVboMesh dokuroMesh;
                    for(auto pos : positions)
                    {
                        dokuroMesh.addNormal( ofVec3f( pos["width"].asFloat() + pos["y"].asFloat()*0.1, 0, 0 ) );
                        dokuroMesh.addVertex( ofVec3f(pos["x"].asFloat() ,0 , pos["y"].asFloat()) );
                    }
                    dokuroMesh.setMode(OF_PRIMITIVE_POINTS);
                    
                    ofColor(0);
                    ofEnablePointSprites();
                    {
                        _shader.begin();
                        _shader.setUniform1i("mirror_mode", 1); // 反転モードフラグ
                        _shader.setUniformTexture("tex0", _texture, 0); //テクスチャの番号。シェーダーで宣言した順番で
                        _shader.setUniformTexture("tex1", _texture, 1);
                        _shader.setUniformTexture("tex2", _texture, 2);
                        _shader.setUniformTexture("tex3", _texture, 3);
                        _shader.setUniformTexture("tex4", _texture, 4);
                        _shader.setUniformTexture("tex5", _texture, 5);
                        _shader.setUniform1f("alpha", 1.0);
                        dokuroMesh.draw();
                        _shader.end();
                    }
                    ofDisablePointSprites();
                    
                    ofColor(255);
                    
                    ofRotateX(90);
                        _plane.drawWireframe();
                    ofRotateX(-90);
                }
                ofPopMatrix();
            
            }
            ofDisableDepthTest();
            _cam.end();
            
            
            // scene name
            ofSetColor(255);
            string msg = "Dokuro";
            ofDrawBitmapString(msg, 10, 40);

            
        }
        getSharedData().fbo.end();
    }
    
    
};
