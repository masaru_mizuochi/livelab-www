uniform sampler2DRect image;
uniform int imgWidth,imgHeight;
uniform int trueWidth,trueHeight;
uniform float rand;
uniform int flags;
uniform vec2 blur_vec;
uniform float val1,val2,val3,val4;
uniform float timer;
uniform float mouseX;
int flgs;
float pix_w,pix_h;
varying vec3 pos;
float volume;



void main (void)
{
	//
    float x, y;
	float scale = 12.0 + volume * 10000.0 ;
	x = floor(pos.x / scale) * scale + scale*0.5;
	y = floor(pos.y / scale) * scale + scale*0.5;

    
//    
    //
    float dist = (pos.x - x) * (pos.x - x) + (pos.y - y) * (pos.y - y);
    if( scale*scale/4.0 < dist && scale*2.0 < x )
    {
        float tmpX = pos.x-x;
        float tmpY = pos.y-y;
        if( 0.0 < tmpX  && 0.0 < tmpY  )
        {
            return;
        }
        else if ( tmpX < 0.0 && tmpY < 0.0 )
        {
            return;
        }
    }
    
    //
	vec2 texCoord = vec2(x , y);
	vec4 col = texture2DRect(image, texCoord);
	
    //
    float r,g,b;
    float scale2 = 4.0;
    float buffer = 0.2;
    r = floor(col.r*scale2) /scale2 + 0.5/scale2 + buffer;
    g = floor(col.g*scale2) /scale2 + 0.5/scale2 + buffer;
    b = floor(col.b*scale2) /scale2 + 0.5/scale2 + buffer;
    
    //
    vec4 col2 = vec4(r, g, b, col.a );

	gl_FragColor = col2;
}
