uniform sampler2DRect image;

uniform float rand;
uniform float noise;
uniform float volume;

varying vec3 pos;

void main (void)
{
    vec2 texCoord = vec2(pos.x , pos.y);


    float noi2 = volume*5.0;
    vec4 col = vec4(0.0,0.0,0.0,1.0); //texture2DRect(image, texCoord);
    vec4 col_r = texture2DRect(image, texCoord + vec2(-50.0*noi2,-12.0*noi2*noi2));
    vec4 col_l = texture2DRect(image, texCoord + vec2( 50.0*noi2*noi2,noi2*noi2));
    vec4 col_g = texture2DRect(image, texCoord + vec2( -27.5*noi2,0));
    

    // col.b = col.b + col_r.b*max(1.0, sin(pos.y*1.2)*2.5)*rand;
    // col.r = col.r + col_l.r*max(1.0, sin(pos.y*1.2)*2.5)*rand;
    // col.g = col.g + col_g.g*max(1.0, sin(pos.y*1.2)*2.5)*rand;


    col.r = col.r + col_l.r*noi2;
    col.b = col.b + col_r.b*noi2;
    col.g = col.g + col_g.g*noi2;
    col = col * 2.5;

    gl_FragColor.rgba = col.rgba;
} 