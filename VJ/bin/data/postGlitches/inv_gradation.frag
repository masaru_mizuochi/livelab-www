uniform sampler2DRect image;
uniform int imgWidth,imgHeight;
uniform int trueWidth,trueHeight;
uniform float rand;
uniform int flags;
uniform vec2 blur_vec;
uniform float val1,val2,val3,val4;
uniform float timer;
uniform float mouseX;
int flgs;
float pix_w,pix_h;
varying vec3 pos;


vec3 hsv_to_rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}


void main (void)
{
	// float x, y;	
	// float scale = 1.0 + mod(floor(timer), 3.0) * 10.0 ;
	// x = floor(pos.x / scale) * scale + scale*0.5;
	// y = floor(pos.y / scale) * scale + scale*0.5;

	vec2 texCoord = vec2(pos.x , pos.y);
	vec4 col = texture2DRect(image, texCoord);;
	
//	float amp = 2.5;
    float amp = 2.5;
	if( floor(col.r*amp) == 0.0  && floor(col.g*amp) == 0.0  && floor(col.b*amp) == 0.0  )
	{
		col.r =
		col.g =
		col.b = 
		col.a = 1.0;
	}
	else
	{
	    float hue =(timer*600.0 - pos.x + pos.y ) / 2500.0;
    	vec3 hsv = vec3(hue, 0.6, 0.8);
    	vec3 col3 = hsv_to_rgb( hsv );
    	col = vec4(col3, 1.0);
	}

	gl_FragColor = col;
}
