#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    // window
    ofEnableAlphaBlending();
    ofSetVerticalSync(true);
    ofBackground(0);
    ofSetFrameRate(60);
    ofSetCircleResolution(30);
    

    // state machine
    _state.addState<Test>();
    _state.addState<Genkidama>();
    _state.addState<Dokuro>();
    _state.addState<Splatoon>();
    _state.addState<RythmGame>();
    _state.addState<Nazotoki>();
    _state.addState<Lyric>();
    changeSceneAt("Test");

    
    // scene names
    _sceneNames["1"] = "Dokuro";
    _sceneNames["2"] = "Nazotoki";
    _sceneNames["3"] = "Splatoon";
    _sceneNames["4"] = "Lyric";
    _sceneNames["5"] = "RythmGame";
    _sceneNames["6"] = "Test";
    _sceneNames["7"] = "Genkidama";
    
    
    // effect name
    _effectNames.push_back( EffectName("effect_rgb", OFXPOSTGLITCH_CONVERGENCE) );
    _effectNames.push_back( EffectName("effect_glow", OFXPOSTGLITCH_GLOW) );
    _effectNames.push_back( EffectName("effect_twist", OFXPOSTGLITCH_TWIST) );
    _effectNames.push_back( EffectName("effect_dot", OFXPOSTGLITCH_DOT) );
    _effectNames.push_back( EffectName("effect_inv_grad", OFXPOSTGLITCH_INV_GRADATION) );
    
    
    // shared data
    _state.getSharedData().fbo.allocate(Const::WIDTH, Const::HEIGHT, GL_RGBA, 4);
    
    
    // osc
    _oscManger.setup();

    //
    _dummyKinectManager.setup();
    
    //
    //_syphon.setName("WWW VJ");
    
    
    // sound
    _sound.listDevices();
    _sound.setup(this, 0, 2, 44100, 256, 4);
    
    
    // postglitch
    _effect.setFbo( & _state.getSharedData().fbo );
}

//--------------------------------------------------------------
void ofApp::update()
{
    // --------- //
    // Maxのパラメータチェック
    // --------- //
    ParamObj p = _oscManger.updateMax();
    if( !p.empty() )
    {
        for (auto e : p)
        {
            _state.getSharedData().params[ e.first ] = e.second;
        }
    }
    
    // Maxのパラメータに変更があったら、設定を変更する系
    if( !_state.getSharedData().params.empty() )
    {
        // シーンの変更
        if( 0 < p.count("scene_id") )
        {
            changeSceneAt( _sceneNames[ ofToString(p["scene_id"]) ] );
        }
        
        // ポストエフェクト編
        for( EffectName e : _effectNames )
        {
            if( 0 < p.count( e.name ) )
            {
                changeEffectSetting( e.name, p[e.name]);
            }
        }
    }
    
    
    // --------- //
    // Kinectのパラメータチェック
    // --------- //
    ofxJSONElement json = _oscManger.updateKinect();
    
    if( json.empty() || p["kinect_dummy"] == 1)
    {
        json = _dummyKinectManager.update();
    }
    
    _state.getSharedData().performerPositions = json;
    
    
    
    // --------- //
    // IRのパラメータチェック
    // --------- //
    vector<Particle> irHands = _oscManger.updateIR();
    _state.getSharedData().irHands = irHands;

}

//--------------------------------------------------------------
void ofApp::draw()
{
    // FBO
    ofSetColor( 255 );
    _effect.generateFx( _state.getSharedData().volume );
    _state.getSharedData().fbo.draw(0, 0);

    
    // FPS
#if DEBUG_MODE != 1
    ofSetColor(255);
    string msg = "fps: " + ofToString(ofGetFrameRate(), 2);
    ofDrawBitmapString(msg, 10, 20);
#endif
    
    
    // syphone
    // _syphon.publishScreen();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    switch( key )
    {
        // fullscreen ⇔ normal window
        case 'f':
            ofToggleFullscreen();
            break;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{

}

//--------------------------------------------------------------
void ofApp::audioIn(float * input, int bufferSize, int nChannels)
{
    // パラメータチェック
    if( _state.getSharedData().params.empty() ) return;
    
    //
    static float volume = 0.0;
    
    for (int i = 0; i < bufferSize; i++)
    {
        volume += input[i]*input[i];
    }
    
    volume /= bufferSize;
    _state.getSharedData().volume = volume * _state.getSharedData().params["volume_gain"];
}

//--------------------------------------------------------------
void ofApp::changeSceneAt(string sceneName)
{
    _state.changeState(sceneName + ".h");
}

//--------------------------------------------------------------
void ofApp::changeEffectSetting(string effectName ,int flag)
{
    bool e_flag = (flag == 0) ? false : true;
    
    for( EffectName effect : _effectNames )
    {
        if( effectName == effect.name )
        {
            _effect.setFx(effect.enumName, e_flag);
        }
    }
}
