//
//  元気玉シーン
//



#pragma once


#include "ofxState.h"
#include "SharedData.h"

#include "Particle3D.h"


class Genkidama : public itg::ofxState<SharedData>
{
public:
    
    // vars
    ofVboMesh _tama;
    ofCamera _cam;
//    ofLight _light;
    
    // vars
    vector<ofImage> _textures; //テクスチャ
    vector<Particle3D> _texs;
    ofShader _shader;
    
    
    // vars for param
    int _pre_framenum = 0;
    int _start_count = 1000;
    int _count = 0;
    
    //
    float _shere_r = 10;
    float _cam_dist = 600;
    float _cam_theta = 0;
    float _cam_theta2 = 0;
    float _cam_theta_delta = 0.01;
    float _cam_theta_delta2 = 0.01;
    
    ofColor _tama_c1 = ofColor(255);
    ofColor _tama_c2 = ofColor(255);
    int _size_max = 30;
    int _size_min = 10;
    
    
    
    string getName()
    {
        return FILE;
    }
    
    void setup()
    {
       // _mesh.setMode(OF_PRIMITIVE_LINE_LOOP);
         _tama.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);

//        _light.enable();
        
        // texture
        reloadTextures(1);
        
        
        // shader
        _shader.load("shaders/pointSprite.vert", "shaders/pointSprite.frag");


        //
        ofSeedRandom();
        for( int i = 0; i < 30; i++ )
        {
            Particle3D p;
            p.setup(i, _cam_dist);
            _texs.push_back(p);
        }
    }
    
    
    void update()
    {
        //別シーンからやってきたら、戻ってる
        if( 10 < ofGetFrameNum() - _pre_framenum )
        {
            _count = 0;
        }
        
        // 定期的に切り替え
        if( ofGetFrameNum() % 1000 == 0 )
        {
            //カメラの動きを切り替え
            ofSeedRandom();
            _cam_theta_delta = (ofRandom(1.0) - 0.5) * 0.1;
            _cam_theta_delta2 = (ofRandom(1.0) - 0.5) * 0.1;
            
        
            _tama.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
//            _mesh.setMode(OF_PRIMITIVE_POINTS);
        }
        
        
        getSharedData().fbo.begin();
        _cam.begin();
        
            // clear
            ofClear(0,0,0,255);
            _tama.clear();
        
            // camera setting
            _cam.setPosition( _cam_dist * sin(_cam_theta) * cos( _cam_theta2 )
                             ,_cam_dist * sin(_cam_theta) * sin( _cam_theta2 )
                             ,_cam_dist * cos(_cam_theta));
            _cam.lookAt( ofVec3f(0,0,0) );

        
            // ---------- //
            // 背景にドットを敷く
            // ---------- //
            ofSeedRandom( int (ofGetFrameNum() / 1000) );
            ofFill();
            int i;
            for( i = 0; i < 200; i++ )
            {
                //
                ( i % 2 == 0 ) ? ofSetColor(230, 256, 167) : ofSetColor(21, 186, 220);
                int rr = 600;
                ofCircle(ofRandom(-rr,rr),
                         ofRandom(-rr,rr),
                         ofRandom(-rr,rr),
                         ofRandom(1,5) );
                
            }

        
            // ---------- //
            // 真ん中にテクスチャが吸い寄せられる
            // ---------- //
            ofSetColor( 255);
            float rate = 0.9;
            ofVboMesh texMesh;
        
            for (i = 0; i < _texs.size(); i++)
            {
                // paricle3D update
                _texs[i].updatePos();
            
                // drawMesh
                texMesh.addNormal( ofVec3f(ofRandom(_size_max,_size_min), int(_texs[i].tex_id % (_textures.size())), 0) ); // size, texture_id
                texMesh.addVertex( ofVec3f(_texs[i].position.x, _texs[i].position.y, _texs[i].position.z ) );
            }
        
            texMesh.setMode(OF_PRIMITIVE_POINTS);
        
            ofEnablePointSprites();
            _shader.begin();
            _shader.setUniform1i("mirror_mode", 0); // 反転モードフラグ
            _shader.setUniformTexture("tex0", _textures[0], 0); //テクスチャの番号。シェーダーで宣言した順番で
            _shader.setUniformTexture("tex1", _textures[1], 1);
            _shader.setUniformTexture("tex2", _textures[2], 2);
            _shader.setUniformTexture("tex3", _textures[3], 3);
            _shader.setUniformTexture("tex4", _textures[4], 4);
            _shader.setUniformTexture("tex5", _textures[5], 5);
            _shader.setUniform1f("alpha", 1.0);
            texMesh.draw();
            _shader.end();
            ofDisablePointSprites();

        
        
            // ---------- //
            // 真ん中の球をかく
            // ---------- //
            ofVboMesh tmpMesh = ofSpherePrimitive(_shere_r, 32).getMesh();
            vector<ofVec3f> vertices = tmpMesh.getVertices();
        
        
            ofSeedRandom();
        
        
            for( ofVec3f points : vertices )
            {
                int rand = 15 + _shere_r * 0.1;
                rand = (ofRandom(1.0) < 0.0025) ? rand*6 : rand;
                //ofVec3f p = ofVec3f( points.x + ofRandom(-rand,rand),  points.y + ofRandom(-rand,rand), points.z+ ofRandom(-rand,rand));
                ofVec3f p = ofVec3f( points.x + (ofNoise(points.x*100, _count*0.1) - 0.5) *rand,
                                     points.y + (ofNoise(points.y*100, _count*0.1) - 0.5) *rand,
                                     points.z + (ofNoise(points.z*100, _count*0.1) - 0.5) *rand);
                
                ofColor c = ( 0.5 < ofRandom(1.0)) ? _tama_c1 : _tama_c2;
                _tama.addColor(c);
                _tama.addVertex(p);
            }
        
            ofSetLineWidth(3);
            _tama.drawWireframe();
//        _mesh.draw();
//        _tama.drawFaces();
        
        //
        _cam.end();
        getSharedData().fbo.end();
        
        
        //update
        _pre_framenum = ofGetFrameNum();
        _count += 1.0;
        
        _cam_theta += _cam_theta_delta;
        _cam_theta2 += _cam_theta_delta2;
    }
    
    
    
    void keyPressed(int key)
    {
        switch(key)
        {
            // ↑
            case 357:
                _shere_r+=5;
                judgeColor();
                break;
            
            // ↓
            case 359:
                _shere_r = (0 < _shere_r) ? _shere_r - 5 : _shere_r ;
                judgeColor();
                break;
                
                // a
            case 'a':
                reloadTextures(1);
                break;
                
                // s
            case 's':
                reloadTextures(2);
                break;
                
                // d
            case 'd':
                reloadTextures(3);
                break;

                
        }
    }
    
    void judgeColor()
    {
        if( 50 <= _shere_r && _shere_r < 150 )
        {
            _tama_c1 = ofColor(200,200,0);
            _tama_c2 = ofColor(255,255,255);
            
            _size_max = 50;
            _size_min = 20;
        }
        else if( 150 <= _shere_r && _shere_r < 250 )
        {
            _tama_c1 = ofColor(200,200,0);
            _tama_c2 = ofColor(200,200,0);
            
            _size_max = 80;
            _size_min = 50;
        }
        else if( 250 <= _shere_r && _shere_r < 350 )
        {
            _tama_c1 = ofColor(255,100,100);
            _tama_c2 = ofColor(200,200,0);
            _size_max = 100;
            _size_min = 80;
        }
        else if( 350 <= _shere_r && _shere_r < 450 )
        {
            _tama_c1 = ofColor(255,100,100);
            _tama_c2 = ofColor(255,100,100);
            
            _size_max = 160;
            _size_min = 100;
        }
    }
    
    void reloadTextures(int textures_id)
    {
        _textures.clear();
        
        ofDirectory dir;
        dir.listDir("textures/genkidama/" + ofToString(textures_id) );
        
        ofDisableArbTex();
        for(int i = 0; i < dir.size(); i++)
        {
            ofImage img;
            img.loadImage(dir.getPath(i));
            _textures.push_back(img);
            
            //cout <<  "texture path:" << dir.getPath(i)  << endl;
        }
        ofEnableArbTex();
    }

  
};
