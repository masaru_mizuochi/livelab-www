//
//  ofxPostGlitch.h
//
//  Created by maxilla inc. on 2013/02/01.
//
//

#ifndef __ofxPostGlitchExample__ofxPostGlitch__
#define __ofxPostGlitchExample__ofxPostGlitch__

#include "ofMain.h"

#define GLITCH_NUM 20

enum ofxPostGlitchType{
	OFXPOSTGLITCH_CONVERGENCE,
	OFXPOSTGLITCH_GLOW,
	OFXPOSTGLITCH_SHAKER,
	OFXPOSTGLITCH_CUTSLIDER,
	OFXPOSTGLITCH_TWIST,
	OFXPOSTGLITCH_OUTLINE,
	OFXPOSTGLITCH_NOISE,
	OFXPOSTGLITCH_SLITSCAN,
	OFXPOSTGLITCH_SWELL,
	OFXPOSTGLITCH_INVERT,
	OFXPOSTGLITCH_CR_HIGHCONTRAST,
	OFXPOSTGLITCH_CR_BLUERAISE,
	OFXPOSTGLITCH_CR_REDRAISE,
	OFXPOSTGLITCH_CR_GREENRAISE,
	OFXPOSTGLITCH_CR_REDINVERT,
	OFXPOSTGLITCH_CR_BLUEINVERT,
	OFXPOSTGLITCH_CR_GREENINVERT,
    OFXPOSTGLITCH_DOT,
    OFXPOSTGLITCH_INV_GRADATION,
    OFXPOSTGLITCH_TEST,
};

class ofxPostGlitch{
public:

	ofxPostGlitch(){
		targetBuffer = NULL;
		shader[0].load("postGlitches/convergence");
		shader[1].load("postGlitches/glow");
		shader[2].load("postGlitches/shaker");
		shader[3].load("postGlitches/cut_slider");
		shader[4].load("postGlitches/twist");
		shader[5].load("postGlitches/outline");
		shader[6].load("postGlitches/noise");
		shader[7].load("postGlitches/slitscan");
		shader[8].load("postGlitches/swell");
		shader[9].load("postGlitches/invert");
		shader[10].load("postGlitches/crHighContrast");
		shader[11].load("postGlitches/crBlueraise");
		shader[12].load("postGlitches/crRedraise");
		shader[13].load("postGlitches/crGreenraise");
		shader[14].load("postGlitches/crRedinvert");
		shader[15].load("postGlitches/crBlueinvert");
		shader[16].load("postGlitches/crGreeninvert");
        shader[17].load("postGlitches/dot");
        shader[18].load("postGlitches/inv_gradation");
        shader[19].load("postGlitches/test");
    }

	/* Initialize & set target Fbo */
	void setup(ofFbo* buffer_);

	/* Set target Fbo */
	void setFbo(ofFbo* buffer_);

	/* Switch each effects on/off */
	void setFx(ofxPostGlitchType type_,bool enabled);

	/* Toggle each effects on/off */
	void toggleFx(ofxPostGlitchType type_);

	/* Return current effect's enabled info*/
	bool getFx(ofxPostGlitchType type_);

	/* Apply enable effects to target Fbo */
	void generateFx(float volume);

protected:
	bool		bShading[GLITCH_NUM];
	ofShader	shader[GLITCH_NUM];
	ofFbo*		targetBuffer;
	ofFbo		ShadingBuffer;
	ofPoint		buffer_size;
	float ShadeVal[4];
};

#endif /* defined(__ofxPostGlitchExample__ofxPostGlitch__) */
