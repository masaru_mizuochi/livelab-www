//
//  定数クラス
//

#pragma once

//デバッグモード。 1:本番／2:デバッグ1(セカンドディスプレイがあるとき)/3:デバッグ2（mac book 単体のとき）
#define DEBUG_MODE 3



// ファイル名
#define FILE (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)


//------//
// typedef
//------//
// maxから送られてくるパラメータはこの型を使う
typedef map<string, float> ParamObj;



//------//
// enum
//------//
//enum MeshType {
//    MESH_DRAW = 0, //塗る
//    MESH_WIREFRAME = 1, //ワイヤで線を引く
//    MESH_ALL = 2 //メッシュで塗り、ワイヤで線を引く
//};



class Const
{
public:
    
    //----------//
    // 本番とデバッグ時で値が異なる系
    //----------//
    
#if DEBUG_MODE == 1
    
    // ウィンドウの幅（セカンドスクリーン）
    const static int WIDTH = 1900;
    
    // ウィンドウの高さ（セカンドスクリーン）
    const static int HEIGHT = 1200;
    
    // ファーストスクリーンの幅
    const static int FIRST_SCREEN_WINDOW_WIDTH = 1920;
    
    
#elif DEBUG_MODE == 2
    
    // ウィンドウの幅（セカンドスクリーン）
    const static int WIDTH = 1900;
    
    // ウィンドウの高さ（セカンドスクリーン）
    const static int HEIGHT = 1900;
    
    // ファーストスクリーンの幅
    const static int FIRST_SCREEN_WINDOW_WIDTH = 1440;

#elif DEBUG_MODE == 3
    
    // ウィンドウの幅（セカンドスクリーン）
    const static int WIDTH = 740;
    
    // ウィンドウの高さ（セカンドスクリーン）
    const static int HEIGHT = 480;
    
    // ファーストスクリーンの幅
    const static int FIRST_SCREEN_WINDOW_WIDTH = 0;
    
#endif
    
    
    //----------//
    // OSC 系
    //----------//
    // Kinect用 受け PORT
    const static int OSC_KINECT_SERVER_PORT = 6000;
    
    // IR用 受け PORT
    const static int OSC_IR_SERVER_PORT = 6001;
    
    // MAX用 受け PORT
    const static int OSC_MAX_SERVER_PORT = 6002;

    // MAX用 攻め PORT
    const static int OSC_MAX_CLIENT_PORT = 6003;

};





