

#pragma once

//デバッグモード 1:本番／2:デバッグ
enum MODES{
	MODE_LIVE = 1,
	MODE_DEBUG = 2,
};


class Const
{
public:

	//---------//
	// DEBUG 系
	//---------//
	const static int MODE = MODE_DEBUG;



    //----------//
    // OSC 系
    //----------//
//    // 受け PORT
//    const static int OSC_SERVER_PORT = 6000;
    
    // 攻め PORT
    const static int OSC_CLIENT_PORT = 6000;
 


    
    //----------//
    // Kinect 系
    //----------//
    // kinectの解像度（よこ）
    const static int WIDTH = 512;
    
    // kinectの解像度（たて）
    const static int HEIGHT = 424;
    
    
    //----------//
    // Layout 系
    //----------//
    // 画像間の幅（よこ）
    const static int W_MARGIN = 20;
    
    // 画像間の幅（たて）
    const static int H_MARGIN = 20;
    


};