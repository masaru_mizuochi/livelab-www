#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
	//-------//
	// window settings
	//-------//
	ofSetFrameRate(30);
	//ofSetVerticalSync(false);


	//-------//
	// set vars
	//-------//
	// Kinect画像の拡大表示率
	SCALE_COLUMN1 = 1.0;

	// OSC Client IP
	OSC_CLIENT_IP = "192.168.10.103";

	// Kinect画像の拡大表示率
	//_bufferSize = Const::WIDTH *  Const::HEIGHT * sizeof( unsigned short );  
	//_bufferMat = cv::Mat(  Const::HEIGHT, Const::WIDTH, CV_16UC1 ); 
	//_depthMat = cv::Mat(  Const::HEIGHT, Const::WIDTH, CV_8UC1 );
	
	// fbo
	_cv_fbo.allocate(Const::WIDTH * SCALE_COLUMN1 , Const::HEIGHT * SCALE_COLUMN1);



	//-------//
	// gui
	//-------//
	_gui.setup("kinect v2 params", "settings.xml", Const::W_MARGIN, Const::HEIGHT * SCALE_COLUMN1 + Const::H_MARGIN*2 );
    _gui.add( _p_kinect_min_dist.set("kinect_min_dist", 1500, 1500, 3000) );
    _gui.add( _p_kinect_max_dist.set("kinect_max_dist", 3000, 1500, 3000) );
    _gui.add( _p_blur.set("kinect_blur", 10, 0, 50) );
    _gui.add( _p_cv_min_area.set("cv_min_area", 10, 0, 50) );
    _gui.add( _p_cv_max_area.set("cv_max_area", 10, 0, 400) );
    _gui.add( _p_cv_threshold.set("cv_threshold", 10, 0, 255) );
    _gui.add( _p_cv_persistance.set("cv_persistance", 10, 0, 100) );
	_gui.add( _p_cv_max_dist.set("cv_max_dist", 10, 0, 100) );
    _gui.add(_p_osc_label.setup("start osc","") );
    _gui.add( _p_start_osc.setup("start_osc") );
        
    _gui.loadFromFile("settings.xml");



	//-------//
	// warper
	//-------//
    int x = Const::W_MARGIN ;
    int y = Const::H_MARGIN;
    int w = Const::WIDTH * SCALE_COLUMN1;
    int h = Const::HEIGHT * SCALE_COLUMN1;
    _warper.setSourceRect(ofRectangle(0, 0, w ,h));
    _warper.setTopLeftCornerPosition(ofPoint(x, y));
    _warper.setTopRightCornerPosition(ofPoint(x + w, y));
    _warper.setBottomLeftCornerPosition(ofPoint(x, y + h));
    _warper.setBottomRightCornerPosition(ofPoint(x + w, y + h));
    _warper.setup();
    _warper.load();

	
	 //-------//
	// kinect 
	//-------//
	setupKinect();


	//-------//
	// osc
	//-------//
    _osc_sender.setup(OSC_CLIENT_IP, Const::OSC_CLIENT_PORT );
	
}


bool ofApp::setupKinect()
{
	HRESULT result = GetDefaultKinectSensor( & _kinect );

	if(  _kinect )
	{
		IDepthFrameSource * depthFrameSource = NULL;

		result = _kinect->Open();
		if(SUCCEEDED(result))
		{
			result = _kinect->get_DepthFrameSource(&depthFrameSource);
		}

		if(SUCCEEDED(result))
		{
			result = depthFrameSource->OpenReader( &_depthFrameReader );
		}
		
		SafeRelease(depthFrameSource);
	}

	//Kinectがみつからない場合
	if(!_kinect || FAILED(result) )
	{
		std::cout << "no kinect sensor found!" << std::endl;
		return false;
	}

	return true;
}


//--------------------------------------------------------------
void ofApp::update()
{
	// Kinectがつながっていない場合
	if(!_kinect )
	{
		std::cout << "no kinect sensor found!" << std::endl;

		// コネクト
		bool is_connect = setupKinect();

		if( !is_connect )
		{
			return;
		}		
	}


	// vars
	IDepthFrame * depthFrame = nullptr;
	IFrameDescription * frameDesc = nullptr;
	USHORT min_dist = _p_kinect_min_dist;
	USHORT max_dist = _p_kinect_max_dist;
	UINT16 * depthBuffer = nullptr;
	
	// 
	HRESULT result = _depthFrameReader->AcquireLatestFrame( &depthFrame );
	
	if (SUCCEEDED(result)) 
	{
		depthBuffer = new UINT16[Const::HEIGHT * Const::WIDTH];
		result = depthFrame->CopyFrameDataToArray(Const::WIDTH * Const::HEIGHT, depthBuffer);
        
		if (SUCCEEDED(result)) 
		{

			cv::Mat depthMap = cv::Mat(Const::HEIGHT, Const::WIDTH, CV_16U, depthBuffer);
			cv::Mat img0 = cv::Mat::zeros(Const::HEIGHT, Const::WIDTH, CV_8UC1);


			int i, j;
			for (i = 0; i < Const::HEIGHT; i++) 
			{
				UINT16 * ptr = depthMap.ptr<UINT16>(i);
				for (j = 0; j <  Const::WIDTH; j++) 
				{
					ptr[j] = ptr[j] - min_dist;
				}
			}

			double scale = 255.0 / (max_dist - min_dist);
			depthMap.convertTo(img0, CV_8UC1, -scale, scale*max_dist ); 

			ofImage tmpImg;
			ofxCv::toOf(img0, tmpImg);
			tmpImg.update();


			// blur
			ofxCv::blur(tmpImg, _img, _p_blur);
			_img.update();


			// clac FPS
			if( Const::MODE == MODE_DEBUG )
			{
				float elapedTime = ofGetElapsedTimeMillis();
				_kinectFps = 1000.0 / ( elapedTime - _preElapsedTime );
				_preElapsedTime = elapedTime;
			}
		}
	}


	// ---------- //
	// contour setting
	// ---------- //
	_contour.setMinAreaRadius( _p_cv_min_area );
	_contour.setMaxAreaRadius( _p_cv_max_area );
	_contour.setThreshold( _p_cv_threshold );
	_contour.getTracker().setPersistence( _p_cv_persistance );
	_contour.getTracker().setMaximumDistance( _p_cv_max_dist );



	// release
	SafeRelease( depthFrame );
	SafeRelease( frameDesc  );
	depthBuffer = nullptr;
	
	//SafeRelease( depthBuffer  );
}

//--------------------------------------------------------------
void ofApp::draw()
{
	
	try
	{

	//-----------------------//
	// clear
	//-----------------------//
	ofSetColor(255);

    // cv_fbo
    _cv_fbo.begin();
    {
        ofClear(0, 255);
    }
    _cv_fbo.end();
    


	//-----------------------//
	// 生データ描画
	//-----------------------//
	_img.draw(Const::W_MARGIN, Const::H_MARGIN, Const::WIDTH * SCALE_COLUMN1, Const::HEIGHT * SCALE_COLUMN1);
	

    // --------//
    // quad warp ui
    // --------//
    ofSetColor(ofColor::magenta);
    _warper.drawQuadOutline();
        
    ofSetColor(ofColor::yellow);
    _warper.drawCorners();
        
    ofSetColor(ofColor::magenta);
    _warper.drawHighlightedCorner();
        
    ofSetColor(ofColor::red);
    _warper.drawSelectedCorner();

	ofSetColor(255);
    

	// --------//
    // fboにwarperのaffine変換をmatrixで適用する
    // --------//
	 ofMatrix4x4 mat = _warper.getMatrixInverse();
        
    _cv_fbo.begin();
    {
        ofClear(0, 0, 0);
            
        glPushMatrix();
        {
            // quad warpの範囲で切り取る
            glMultMatrixf( mat.getPtr() );
                
            // draw
            _img.draw( Const::W_MARGIN  ,
                       Const::H_MARGIN,
                       Const::WIDTH * SCALE_COLUMN1,
                       Const::HEIGHT * SCALE_COLUMN1 );
        }
        glPopMatrix();
    }
    _cv_fbo.end();
	_cv_fbo.draw( Const::WIDTH * SCALE_COLUMN1 +  Const::H_MARGIN * 2, Const::H_MARGIN );



	 // --------//
    // contour finder
    // --------//
	//輪郭を抽出
    ofPixels pixels;
    _cv_fbo.readToPixels(pixels);
	ofImage cv_img;
	cv_img.setFromPixels( pixels );
	cv_img.setFromPixels( pixels );
	_contour.findContours( cv_img );
	
    
	//輪郭を描画
	 ofxJSONElement json;
	 glPushMatrix();
    {
        glTranslated( Const::WIDTH * SCALE_COLUMN1 +  Const::H_MARGIN * 2, Const::H_MARGIN, 0);
        
        //
        _contour.draw();
        
        // get tracking logs
        ofxCv::RectTracker& tracker = _contour.getTracker();
        int len = _contour.size();
        for(int contour_id = 0; contour_id < len; contour_id++)
        {
            // get label
            unsigned int label = _contour.getLabel( contour_id );
            
            // color
            ofSeedRandom( label << 24 );
            ofSetColor( ofColor::fromHsb(ofRandom(255), 255, 255) );
            
            // 現在のposition
            const cv::Rect& current = tracker.getCurrent(label);
            ofVec2f pos = ofVec2f( current.x + current.width*0.5, current.y + current.height*0.5);

            // labelの描画
            stringstream ss;
            ss << label <<  ":" << tracker.getAge(label) ;
            ofDrawBitmapString(ss.str(), pos.x, pos.y);

			 // json
            ofxJSONElement ele;
            ele["id"] = label;
            ele["x"] = current.x;
            ele["y"] = current.y;
			ele["width"] = current.width;
			ele["height"] = current.height;
			json[ ofToString(label) ] = ele;


			//cout <<label << ":" << current.x  << endl;
        }
    }
    glPopMatrix();

	//cout << "----"  << endl;
	
	
	// --------//
    // ui
    // --------//
	_gui.draw();


	// --------//
	//framerate
	// --------//
	ofSetColor(0);
	stringstream ss;
	ss << "FPS(display):" << ofToString( ofGetFrameRate(), 0) << endl;
	ss << "FPS(kinect):" << ofToString( _kinectFps, 0) ;
	ofDrawBitmapString( ss.str(), Const::WIDTH *0.5 + Const::W_MARGIN*2, Const::HEIGHT + Const::H_MARGIN*3 );


	// --------//
	// oscで送る
	// --------//
    if( _p_start_osc )
    {
        ofxOscMessage m;
        m.setAddress("/data");
        m.addStringArg(json.getRawString());

		//cout << json.getRawString() << endl;

        _osc_sender.sendMessage(m);
    }

	//cout << "-----------------"  << endl;


	// clear
	cv_img.clear();
	pixels.clear();

	}
	catch(std::bad_alloc e)
	{
		abort();
		//exit();
	}
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
	switch (key)
    {
        case 's':
           _warper.save();
            break;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
