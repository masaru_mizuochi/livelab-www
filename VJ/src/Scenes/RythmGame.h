//
//  お客さんの腕の動きのリズムで音ヲタゲー風映像シーン
//


#pragma once

#include "ofxState.h"

class RythmGame : public itg::ofxState<SharedData>
{
public:
    
  
    
    string getName()
    {
        return FILE;
    }
    
    void setup()
    {
        
    }
    
    void update()
    {
        getSharedData().fbo.begin();
        {
            
            // clear
            ofBackground(0);
            
            
            // IRの位置を取得
            vector<Particle> irHands = getSharedData().irHands;
            ofColor(255);
            
            for(Particle irHand : irHands)
            {
                ofCircle( irHand.position.x, irHand.position.y, 10.0);
            }
            
            //
            ofSetColor(255);
            string msg = "rythm game";
            ofDrawBitmapString(msg, 10, 40);
        }
        getSharedData().fbo.end();
    }
    
    
};
