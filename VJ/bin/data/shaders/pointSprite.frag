#version 120

uniform int mirror_mode;
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;
uniform sampler2D tex4;
uniform sampler2D tex5;
//uniform sampler2D tex6;
//uniform sampler2D tex7;
//uniform sampler2D tex8;
//uniform sampler2D tex9;

uniform float alpha;

varying float textureNumber;


void main() 
{
   
    vec2 st = gl_TexCoord[0].st;
    
    if( 0 < mirror_mode )
    {
        st = vec2( st.x, 1.0 - st.y );
    }
    
    
    vec4 color = vec4(255);


    int num = int(textureNumber);
    
    if(num == 0) color = texture2D(tex0, st);
    else if(num == 1) color = texture2D(tex1,  st);
    else if(num == 2) color = texture2D(tex2,  st);
    else if(num == 3) color = texture2D(tex3,  st);
    else if(num == 4) color = texture2D(tex4,  st);
    else if(num == 5) color = texture2D(tex5,  st);
//    else if(num == 6) color = texture2D(tex6,  st);
//    else if(num == 7) color = texture2D(tex7,  st);
//    else if(num == 8) color = texture2D(tex8,  st);
//    else if(num == 9) color = texture2D(tex9,  st);

    color.a = (color.a == 0 ) ? 0 : alpha;

  
    gl_FragColor =  color * gl_Color;
    
}

