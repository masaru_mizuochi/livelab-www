//
//  テストシーン
//


#pragma once

#include "ofxState.h"

class Test : public itg::ofxState<SharedData>
{
public:

    // vars for test
    ofBoxPrimitive _box;
    ofConePrimitive _cone;
    ofSpherePrimitive _spere;
    
    
    string getName()
    {
        return FILE;
    }
    
    void setup()
    {
        
    }
    
	void update()
    {
        getSharedData().fbo.begin();
        {
        
            ofBackground(0);
            float frame_num = ofGetFrameNum();
            float scale = 100 * getSharedData().volume  + 1.0;
            glLineWidth(1.0);
            
            
            //-------//
            // ひだり
            //-------//
            ofSetColor(255, 0, 0);
        
            ofPushMatrix();
            {
                ofTranslate(Const::WIDTH*0.5/3, Const::HEIGHT*0.5, 0);
                ofScale(scale,scale, scale);
                ofRotate(frame_num, 1.0, 1.0, 0.0);
                
                _box.set(50);
                _box.drawWireframe();
            }
            ofPopMatrix();
            
        
            
            //-------//
            // まんなか
            //-------//
            ofSetColor(0, 255, 0);
        
            ofPushMatrix();
            {
                ofTranslate(Const::WIDTH*1.5/3, Const::HEIGHT*0.5, 0);
                ofScale(scale,scale, scale);
                ofRotate(frame_num, 1.0, 0.0, 1.0);
                
                _cone.set(50, 100, 20, 20);
                _cone.drawWireframe();
            }
            ofPopMatrix();
            
    
            //-------//
            // みぎ
            //-------//
            ofSetColor(0, 0, 255);

            ofPushMatrix();
            {
                ofTranslate(Const::WIDTH*2.5/3, Const::HEIGHT*0.5, 0);
                ofScale(scale, scale, scale);
                ofRotate(frame_num, 0.0, 1.0, 1.0);
                
                _spere.setRadius(50);
                _spere.drawWireframe();
            }
        }
        getSharedData().fbo.end();
    }

    
};
