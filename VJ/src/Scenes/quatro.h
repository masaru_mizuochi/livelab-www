//
//  クアトロで使ったクエスチョンパワーが溜まるシーン
//

#pragma once

#include "ofxState.h"
#include "SharedData.h"

#include "Particle.h"

class Quatro : public itg::ofxState<SharedData>
{
public:
    
    
    // params
    int _alive_timing = 5;
    int _life_count = 50;
    float _rect_rate = 0.8;
    int _max_size = 200;
    float _max_vec = 3.0;
    ofFloatColor _color;
    
    // bool
    bool _is_audioreactive = false;
    
    
    // vars
    ofVboMesh _mesh;
    ofShader _shader;
//    ofImage _dot;
    vector<ofImage> _textures;
    vector<float> _pointSizes;
    vector<Particle> _particles;
    ofRectangle _rect;
    

    
    
    // 一時的な変数
    ofVec3f _p;
    
    

    void setup()
    {
        // settings
        ofBackground(200, 0, 0);
        glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
        ofSetFrameRate(60);
        
        // load images
        ofDirectory dir;
        dir.listDir("textures/icon");
        ofDisableArbTex();
        for(int i = 0; i < dir.size(); i++)
        {
            ofImage img;
            img.loadImage(dir.getPath(i));
            _textures.push_back(img);
        }
        ofEnableArbTex();
        
        
        // load shader
        _shader.load("shaders/pointSprite.vert", "shaders/pointSprite.frag");
        
        // settings mesh
        _mesh.setMode(OF_PRIMITIVE_POINTS);
        
        // set rect
        _rect = ofRectangle( ofGetWidth() * (1 - _rect_rate) * 0.5 , ofGetHeight() * (1 - _rect_rate) * 0.5, ofGetWidth() * _rect_rate , ofGetHeight() * _rect_rate);

        // set color
        _color.setHsb(0.0, 1.0, 1.0);
        
        
        
        ///////////
//        OFX_REMOTEUI_SERVER_SETUP();
//        OFX_REMOTEUI_SERVER_SHARE_PARAM(_alive_timing, 1, 80);
//        OFX_REMOTEUI_SERVER_SHARE_PARAM(_life_count, 50, 1000);
//        OFX_REMOTEUI_SERVER_SHARE_PARAM(_max_size, 30, 500);
//        OFX_REMOTEUI_SERVER_SHARE_PARAM(_max_vec, 0, 10);
//        OFX_REMOTEUI_SERVER_SHARE_PARAM(_is_audioreactive, 0, 1);
//        OFX_REMOTEUI_SERVER_SET_DRAWS_NOTIF(false);
    }
    
	void update()
    {
        int frame_num = ofGetFrameNum();
        
        _alive_timing = ( frame_num % 500 == 0 && 3 < _alive_timing) ? _alive_timing - 1.0 : _alive_timing;
        _life_count = ( frame_num % 500 == 0 ) ? _life_count + 5.0 : _life_count;
        
        
        // particle生まれる
        if (frame_num % _alive_timing == 0)
        {
            ofVec2f pos = ofVec2f( _rect.x + _rect.width * ofRandom(0, 1) , _rect.y + _rect.height *  ofRandom(0, 1));
            ofVec2f vec = ofVec2f( ofRandom(-_max_vec, _max_vec), ofRandom(-_max_vec, _max_vec) );
            
            
            Particle p;
            p.friction = 0.0005;
            float size = ofRandom(30, _max_size );
            int r = ofRandom(0, 50);
            int texture_id = (45 <= r) ? r - 44 : 0;
            p.setup(pos, vec, _color, size,  _life_count, texture_id);
            _particles.push_back(p);
            
            
        }
        
        // particleの位置を制御
        int len = _particles.size();
        for( int i = 0; i < len ; i++ )
        {
            
            //
            _particles[i].update();
            
            // 力をリセット
            _particles[i].resetForce();
            
            
            // 引力の位置
            float x = ofNoise(frame_num*0.01) * _rect.width + _rect.x;
            float y = ofNoise(frame_num*0.02) * _rect.height + _rect.y;
            float power = abs(ofNoise(frame_num*0.002)) * _max_vec ;
            _particles[i].addAttractionForce( x,  y, ofGetWidth(), power);
            _p = ofVec3f(x,y, power);
            
            // 壁との跳ね返り
            _particles[i].bounceOffWalls(_rect);
            
            
            
            //
            if( _life_count <= _particles[i].count )
            {
                _particles.erase(_particles.begin() + i);
            }
        }
        
        
        // できたら使いまわしたいな。
        len = _particles.size();
        _mesh.getVertices().resize(len);
        _mesh.getColors().resize(len);
        _mesh.getNormals().resize(len);
        for( int i = 0; i < len ; i++ )
        {
            //
            _mesh.getVertices()[i].x = _particles[i].position.x; //ofRandom(0,ofGetWidth());
            _mesh.getVertices()[i].y = _particles[i].position.y; //ofRandom(0,ofGetHeight());
            _mesh.getColors()[i].r = _particles[i].color.r;
            _mesh.getColors()[i].g = _particles[i].color.g;
            _mesh.getColors()[i].b = _particles[i].color.b;
            _mesh.getNormals()[i].x = (_is_audioreactive) ? _particles[i].size * (getSharedData().volume + 0.1) : _particles[i].size ; //大きさ
            _mesh.getNormals()[i].y =  _particles[i].texture_id ; //テクスチャのID
            
            
            //cout << _particles[i].color.r << "," << _particles[i].color.g << "," << _particles[i].color.b << endl;
        }
    
        
        //---------//
        // draw
        //---------//
        
        getSharedData().myFbo.begin();
        
        
            // clear
            ofClear(0);
            
            
            // 白い枠を描画
            ofSetColor(255);
            ofNoFill();
            ofSetLineWidth(20);
            ofRect( _rect.x , _rect.y, _rect.width , _rect.height);
            ofFill();

        
            // パーティクルの描画
            ofEnablePointSprites();
                _shader.begin();
                _shader.setUniformTexture("tex0", _textures[0], 0);
                _shader.setUniformTexture("tex1", _textures[1], 1);
                _shader.setUniformTexture("tex2", _textures[2], 2);
                _shader.setUniformTexture("tex3", _textures[3], 3);
                _shader.setUniformTexture("tex4", _textures[4], 4);
                _shader.setUniformTexture("tex5", _textures[5], 5);
                _mesh.draw();
            _shader.end();
            ofDisablePointSprites();
        
        
//            //引力の位置
//            ofSetColor(255);
//            ofCircle(_p.x, _p.y, _p.z*30);
        
//            // FPS
//            ofSetColor(255);
//            stringstream ss;
//            ss << "framerate: " << ofToString(ofGetFrameRate(), 0);
//            ofDrawBitmapString(ss.str(), 10, 20);
        
        getSharedData().myFbo.end();
    }
    
	string getName()
    {
        return FILE;
    }
    
    
    
};
